import {usersConstants} from "../constants/users.constants";

export const usersActions = {
    getAllUsers
}

function getAllUsers(){
    //peticion a cualquier origen de datos
    return dispatch => {
        // dispara la accion de peticion -> por temas de debugueo
        dispatch(request());

        // peticion get
        const users = [
            {name:"Daniel",apellido:"el travieso"},
            {name:"Pastor",apellido:"aleman"},
            {name:"Ivan",apellido:"el terrible"},
            {name:"Carlos",apellido:"v"},
        ];

        //dispara la accion de exito de la peticion
        dispatch(success(users));
        //try {
        //    
        //} catch () {
        //    
        //}
    }

    // funcion que regresa la accion de peticio
    function request(){
        return {
            type: usersConstants.GET_ALL_USER_REQUEST,
        }
    }

    //funcion que regresa la accion de exito con los datos del servidor
    function success(users){
        return {
            type:usersConstants.GET_ALL_USER_REQUEST_SUCCESS,
            payload:users
        }
    }
}