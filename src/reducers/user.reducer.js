import {usersConstants} from "../constants/users.constants";

const initialState = {
    allUser:[]
}

/**
 * 
 * @param {object} state 
 * @param {object} action
 */
export default function userReducer(state = initialState, action){
    switch(action.type){
        case usersConstants.GET_ALL_USER_REQUEST:
            return {...state}
        case usersConstants.GET_ALL_USER_REQUEST_SUCCESS:
            return {...state, allUser:action.payload}
        default:
            return {...state}
    }
}