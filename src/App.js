import React from 'react';
import logo from './logo.svg';
import './App.css';

//conexion componente con redux
import {connect} from "react-redux";
//importar acciones de usuarios
import {usersActions} from "./actions/users.actions";

//bs
import {ListGroup} from "react-bootstrap";

class App extends React.Component {

  constructor(props){
    super(props);
    //permite accceder a el contenido de la clase ej: props y state
    this.getAllUsers = this.getAllUsers.bind(this);
  }

  getAllUsers(){
    this.props.dispatch(usersActions.getAllUsers());
  }

  render(){
    const {users} = this.props;
    //const listUsers = users.allUser? users.allUser:[];
    console.log("Store redux", users);

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          
          <button onClick={this.getAllUsers} >
            obtener usuarios
          </button>

          <ListGroup>
            {
              users.allUser.map((user, index) => {
                return (
                  //jsx
                  <ListGroup.Item key={index} >{user.name + " " +user.apellido}</ListGroup.Item>
                )
              })
            }
          </ListGroup>
        </header>
      </div>
    );
  }
}

//funcion para conetar y que te permita usar datos del storage
function mapsPropsState(state){
  return {
    users:state.users,
  }
}

export default connect(mapsPropsState) (App);
