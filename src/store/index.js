import {createStore, combineReducers, applyMiddleware} from "redux";
// lib para acciones asyncronas
import thunkMiddleware from "redux-thunk";
import {createLogger} from "redux-logger";
import userReducer from "../reducers/user.reducer";

const loggerMiddleware = createLogger();


const rootReducers = combineReducers(
    {
        users: userReducer
    }
);

// creacion del store
export const store = createStore(rootReducers, applyMiddleware(loggerMiddleware, thunkMiddleware));